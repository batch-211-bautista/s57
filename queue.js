let collection = [];
// Write the queue functions below. 


//Gives all the elements of the queue
function print(){
    return (collection);
}

//Adds an element/elements to the end of the queue
function enqueue(item){
    collection[collection.length] = item;
    return collection;
}

//Removes an element in front of the queue
function dequeue(){
    for (let ctr = 0; ctr < collection.length; ctr++) {
        collection.splice(ctr, 1);
        break;
    }
    return collection;
}


//Shows the element at the front
function front(){
    for (let ctr = 0; ctr < collection.length; ctr++) {
        return collection[ctr];
    }
}

//Shows the total number of elements
function size(){
    for (let ctr = 0; ctr <= collection.length; ctr++) {
        if (ctr == collection.length){
            return ctr;
        }
    }
}

//Gives a Boolean value describing whether the queue is empty or not
function isEmpty(){
    for (let ctr = 0; ctr <= collection.length; ctr++) {
        if (ctr == collection.length){
            return (ctr > 0) ? false : true;
        }
    }
}








// Export create queue functions below.
module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};